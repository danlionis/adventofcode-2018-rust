use file_reader;
use std::collections::HashMap;
use std::i32;

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
struct Point(i32, i32);

struct Grid {
    points: Vec<Point>,
    values: HashMap<Point, i32>,
    size: (i32, i32),
}

enum Border {
    Top,
    Right,
    Bottom,
    Left,
}

#[derive(Debug)]
enum Edge {
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
}

impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point(x, y)
    }

    /// Calculate the Manhattan distance from another point
    fn distance(self, other: &Point) -> i32 {
        (self.0 - other.0).abs() + (self.1 - other.1).abs()
    }

    /// Check what Edge the point is closest to
    ///
    /// return the Edge and the distance
    fn closest_edge(&self, x: i32, y: i32) -> (Edge, i32) {
        let tl = Point::new(0, 0).distance(&self);
        let tr = Point::new(x, 0).distance(&self);
        let bl = Point::new(0, y).distance(&self);
        let br = Point::new(x, y).distance(&self);

        let mut current_edge = Edge::TopLeft;
        let mut current_dist = tl;

        if tr < current_dist {
            current_edge = Edge::TopRight;
            current_dist = tr;
        }
        if bl < current_dist {
            current_edge = Edge::BottomLeft;
            current_dist = bl;
        }
        if br < current_dist {
            current_edge = Edge::BottomRight;
            current_dist = br;
        }

        // println!("{} {} {} {}", tl, tr, bl, br);
        // println!("{:?} {:?}\n", self, current_edge);

        (current_edge, current_dist)
    }
}

impl Default for Point {
    fn default() -> Point {
        Point(0, 0)
    }
}

pub fn day06() {
    let input = file_reader::read_input("input/day06.txt").unwrap();
    let mut points = parse_input(&input);
    print_points(&points);
    points.sort_by_key(|k| k.0);
    points.sort_by_key(|k| k.1);
    println!("{:?}", closest_points_to_edges(&points));
}

fn closest_points_to_edges(points: &Vec<Point>)-> [Point; 4] {
    let (x, y) = find_max_xy(&points);
    let mut curr_tl_dist: i32 = i32::MAX;
    let mut curr_tr_dist: i32 = i32::MAX;
    let mut curr_bl_dist: i32 = i32::MAX;
    let mut curr_br_dist: i32 = i32::MAX;
    let mut curr_tl_point: Point = Point::default();
    let mut curr_tr_point: Point = Point::default();
    let mut curr_bl_point: Point = Point::default();
    let mut curr_br_point: Point = Point::default();
    for &p in points.iter() {
        let (edge, dist) = p.closest_edge(x, y);
        match edge {
            Edge::TopLeft => {
                if dist < curr_tl_dist {
                    curr_tl_dist = dist;
                    curr_tl_point = p;
                }
            }
            Edge::TopRight => {
                if dist < curr_tr_dist {
                    curr_tr_dist = dist;
                    curr_tr_point = p;
                }
            }
            Edge::BottomLeft => {
                if dist < curr_bl_dist {
                    curr_bl_dist = dist;
                    curr_bl_point = p;
                }
            }
            Edge::BottomRight => {
                if dist < curr_br_dist {
                    curr_br_dist = dist;
                    curr_br_point = p;
                }
            }
        }
    }

    // println!("tl {} {:?}", curr_tl_dist, curr_tl_point);
    // println!("tr {} {:?}", curr_tr_dist, curr_tr_point);
    // println!("bl {} {:?}", curr_bl_dist, curr_bl_point);
    // println!("br {} {:?}", curr_br_dist, curr_br_point);

    [curr_tl_point, curr_tr_point, curr_bl_point, curr_br_point]
}

fn parse_input(input: &Vec<String>) -> Vec<Point> {
    let mut points: Vec<Point> = Vec::new();
    for line in input.iter() {
        let parsed: Vec<i32> = line
            .split(", ")
            .map(|s| s.parse::<i32>().unwrap())
            .collect();
        let point = Point(parsed[0], parsed[1]);
        points.push(point);
    }
    points
}

fn part1(points: &Vec<Point>) {
    let max_x: i32 = points.iter().map(|p| p.0).max().unwrap();
    let max_y: i32 = points.iter().map(|p| p.1).max().unwrap();
    let mut grid: HashMap<Point, i32> = HashMap::new();
    println!("{} {}", max_x, max_y);

    for i in 0..max_x {
        for j in 0..max_y {
            let testing_point = Point(i, j);

            match get_closest_point(&points, &testing_point) {
                Some(p) => {
                    *grid.entry(p).or_insert(0) += 1;
                }
                _ => println!("{}/{}", i, j),
            }
        }
    }

    // println!("{:#?}", grid);
    // println!("{:?}", grid.values().max());
    // closest_points_to_edges(&points);

    // for i in 0..points.len() {
    //     for j in i + 1..points.len() {}
    // }
}

fn get_closest_point(points: &Vec<Point>, test: &Point) -> Option<Point> {
    let mut min: i32 = i32::MAX;
    let mut current: Point = Point::default();
    for i in 0..points.len() {
        let p = points[i];
        // if p == *test {
        //     return None;
        // }

        let dist = p.distance(&test);
        if dist < min {
            min = dist;

            if p == current {
                return None;
            }

            current = p;
        }
    }
    Some(current)
}

fn print_points(points: &Vec<Point>) {
    let mut points = points.clone();
    let max_x: i32 = points.iter().map(|p| p.0).max().unwrap();
    let max_y: i32 = points.iter().map(|p| p.1).max().unwrap();
    let mut i: usize = 0;
    points.sort_by_key(|k| k.0);
    points.sort_by_key(|k| k.1);

    let add = 1;

    for y in 0..max_y + add {
        'inner: for x in 0..max_x + add {
            let p1 = Point(x, y);
            let p2 = match points.get(i) {
                Some(p) => p,
                _ => {
                    print!(".");
                    continue;
                }
            };
            if p1 == *p2 {
                print!("{}", (i + 65) as u8 as char);
                i += 1;
            } else {
                print!(".");
            }
        }
        print!("\n");
    }
}

fn find_max_xy(points: &Vec<Point>) -> (i32, i32) {
    let max_x: i32 = points.iter().map(|p| p.0).max().unwrap();
    let max_y: i32 = points.iter().map(|p| p.1).max().unwrap();
    (max_x, max_y)
}
