#![allow(dead_code, unused_imports)]

mod day06;
use day06::*;
mod day05;
use day05::*;
mod day04;
use day04::*;
mod day03;
use day03::*;
mod day02;
use day02::*;
mod day01;
use day01::*;
mod file_reader;

fn main() {
    let day = std::env::args().skip(1).take(1).collect::<String>();
    match &day[..] {
        "01" => day01(),
        "02" => day02(),
        "03" => day03(),
        "04" => day04(),
        "05" => day05(),
        "06" => day06(),
        _ => println!("day not found")
    }
}
