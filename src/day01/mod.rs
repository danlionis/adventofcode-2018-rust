#![allow(dead_code)]

use std::collections::HashSet;
use std::fs;

pub fn day01() {
  let data = fs::read_to_string("input/day01.txt").expect("Unable to read file");
  let contents: Vec<&str> = data.split("\r\n").collect::<Vec<&str>>();
  let numbers: Vec<i32> = contents.iter().map(|n| n.parse::<i32>().unwrap()).collect();

  println!("1/1 {}", part1(&numbers).unwrap());

  println!("1/2 {}", part2(&numbers).unwrap())
}

fn part1(freq: &Vec<i32>) -> Option<i32> {
  Some(freq.iter().sum())
}

fn part2(freq: &Vec<i32>) -> Option<i32> {
  let mut starting: i32 = 0;

  let mut duplicates: HashSet<i32> = HashSet::new();
  let mut found: Option<i32> = None;
  loop {
    starting = freq.iter().fold(starting, |sum, val| {
      if duplicates.contains(&sum) && found == None {
        found = Some(sum);
      }
      duplicates.insert(sum);

      sum + val
    });
    if found != None {
      return found;
    }
  }
}