use std::i32;

pub fn day05() {
  let input = std::fs::read_to_string("input/day05.txt").unwrap();
  part1(&input);
  // part2(&input);
}

fn part1(input: &String) {
  let mut chars: Vec<char> = input.chars().collect();
  let end = react(&mut chars);
  println!("5/1 {}", end);
}

fn part2(input: &String) {
  let chars: Vec<char> = input.chars().collect();
  let filter = "abcdefghijklmnopqrstuvwxyz".chars().collect::<Vec<char>>();
  let mut shortest: i32 = i32::MAX;

  for c in filter {
    let temp = chars.clone();
    let mut filtered: Vec<char> = temp
      .into_iter()
      .filter(|&a| a.to_lowercase().to_string() != c.to_string())
      .collect();
    let end = react(&mut filtered);
    if end < shortest {
      shortest = end;
    }
  }
  println!("5/2 {}", shortest);
}

fn react(chars: &mut Vec<char>) -> i32 {
  let mut i = 0;


  while i < chars.len() - 1 {
    let a: char = chars[i];
    let b: char = chars[i + 1];

    if a.to_lowercase().to_string() == b.to_lowercase().to_string() {
      if (a.is_lowercase() && b.is_uppercase()) || (b.is_lowercase() && a.is_uppercase()) {
        chars.remove(i);
        chars.remove(i);

        // subtract 1, set to 0 if overflow occured
        i = i.checked_sub(1).unwrap_or(0);

        continue;
      }
    }

    i += 1;
  }

  chars.len() as i32
}
