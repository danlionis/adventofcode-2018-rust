use file_reader;

use std::fs;

pub fn day04() {
  let mut data = file_reader::read_input("input/day04.txt").unwrap();
  data.sort();
  println!("{:?}", data);
  // fs::write("input/day04.1.txt", data.join("\r\n")).unwrap();
  // println!("{:?}", data);
  parse_input(&data);
}

fn parse_input(input: &Vec<String>) {
  for entry in input.iter() {
    let parts = entry
      .split_whitespace()
      .map(|s| s.to_owned())
      .collect::<Vec<String>>();
    // println!("{:?}", parts);

    let date = parse_date(&parts[0]);
    let time = parse_time(&parts[1]);
    if parts.len() == 6 {
      let guard = parse_guard(&parts[3]);
      println!("{:?} : {:?} -> {}", date, time, guard);
    }
  }
}

fn parse_date(raw: &String) -> (u32, u32, u32) {
  let date: String = raw.chars().skip(1).collect();
  let parts: Vec<u32> = date.split("-").map(|s| s.parse::<u32>().unwrap()).collect();
  let date = (parts[0], parts[1], parts[2]);
  date
}

fn parse_time(raw: &String) -> (u32, u32) {
  let time: String = raw.chars().take(5).collect();
  let parts: Vec<u32> = time.split(":").map(|s| s.parse::<u32>().unwrap()).collect();
  let time = (parts[0], parts[1]);
  time
}

fn parse_guard(raw: &String) -> u32 {
  let id: u32 = raw
    .chars()
    .skip(1)
    .collect::<String>()
    .parse::<u32>()
    .unwrap();

  id
}
