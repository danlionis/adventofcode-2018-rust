pub struct Event {
  guard: u32,
  timestamp: u64,
  event: EventKind,
}

pub enum EventKind {
  ShiftBegin,
  FallingAsleep,
  WakingUp,
}
