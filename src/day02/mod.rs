#![allow(dead_code)]

use std::collections::HashMap;
use std::fs;

pub fn day02() {
  let data = fs::read_to_string("input/day02.txt").expect("Unable to read file");
  let input: Vec<String> = data
    .split("\r\n")
    .map(|s| s.to_owned())
    .collect::<Vec<String>>();
  part1(&input);
  println!("2/1 {}", part1(&input));
  println!("2/2 {}", part2(&input));
}

fn part1(input: &Vec<String>) -> u32 {
  // let mut ex2: u32 = 0;
  // let mut ex3: u32 = 0;
  let (mut ex2, mut ex3) = (0, 0);
  for s in input {
    let temp = count(&s);
    ex2 += temp.0;
    ex3 += temp.1;
  }
  ex2 * ex3
}

fn count(input: &String) -> (u32, u32) {
  let mut found: HashMap<char, u32> = HashMap::new();

  for c in input.chars() {
    *found.entry(c).or_insert(0) += 1;
  }

  let mut exactly_2 = false;
  let mut exactly_3 = false;

  for (_, &v) in found.iter() {
    if v == 2 {
      exactly_2 = true;
    }
    if v == 3 {
      exactly_3 = true;
    }
  }

  (if exactly_2 { 1 } else { 0 }, if exactly_3 { 1 } else { 0 })
}

fn part2(input: &Vec<String>) -> String {
  for i in 0..input.len() {
    for j in i + 1..input.len() {
      if let Some(common) = common_chars(&input[i].to_owned(), &input[j].to_owned()) {
        return common;
      }
    }
  }

  return "".to_string();
}

fn common_chars(a: &str, b: &str) -> Option<String> {
  let mut one_wrong = false;
  for (ca, cb) in a.chars().zip(b.chars()) {
    if ca != cb {
      if one_wrong {
        return None;
      }
      one_wrong = true;
    }
  }

  let new: String = a
    .chars()
    .zip(b.chars())
    .filter(|(ca, cb)| ca == cb)
    .map(|(c, _)| c)
    .collect();

  Some(new)
}
