use std::fs;

pub fn read_input(src: &str) -> Result<Vec<String>, std::io::Error> {
  let data = fs::read_to_string(src)?;
  let input: Vec<String> = data
    .split("\r\n")
    .map(|s| s.to_owned())
    .collect::<Vec<String>>();
  return Ok(input);
}
