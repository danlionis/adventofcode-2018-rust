mod claim;

use self::claim::*;
use file_reader;
use std::collections::HashMap;

pub fn day03() {
  let data = file_reader::read_input("input/day03.txt").expect("Cannot read file");
  let mut claims: Vec<Claim> = Vec::new();
  for i in data {
    claims.push(parse_input(&i));
  }
  let grid = part1(&claims);
  part2(&claims, &grid);
}

type Grid = HashMap<(u32, u32), u32>;

fn part1(claims: &Vec<Claim>) -> Grid {
  let mut grid: Grid = Grid::new();

  for c in claims {
    for p in c.points() {
      *grid.entry(p).or_insert(0) += 1;
    }
  }

  let mut conflicts = 0;
  let mut biggest = 0;

  for &p in grid.values() {
    if p > biggest {
      biggest = p;
    }
    if p >= 2 {
      conflicts += 1;
    }
  }
  println!("biggest {}", biggest);
  println!("3/1 {}", conflicts);

  grid
}

fn part2(claims: &Vec<Claim>, grid: &HashMap<(u32, u32), u32>) {

  for c in claims {
    if c.points().iter().all(|p| grid[&p] == 1) {
      println!("3/2 {}", c.id);
    }
  }
}

fn parse_input(claim: &String) -> Claim {
  let parts: Vec<String> = claim
    .split(char::is_whitespace)
    .map(|s| s.to_owned())
    .collect();
  let id: u32 = parts[0]
    .chars()
    .skip(1)
    .collect::<String>()
    .parse::<u32>()
    .unwrap();
  let pos: Vec<u32> = parts[2]
    .split(",")
    .map(|s| s.split(":").collect::<String>())
    .map(|s| s.parse::<u32>().unwrap())
    .collect();
  let dimensions: Vec<u32> = parts[3]
    .split("x")
    .map(|s| s.parse::<u32>().unwrap())
    .collect();

  Claim {
    id,
    left: pos[0],
    top: pos[1],
    width: dimensions[0],
    height: dimensions[1],
  }
}
