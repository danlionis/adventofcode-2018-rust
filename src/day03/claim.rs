#![allow(dead_code)]

#[derive(Debug, Copy, Clone)]
pub struct Claim {
  pub id: u32,
  pub left: u32,
  pub top: u32,
  pub width: u32,
  pub height: u32,
}

type Point = (u32, u32);

impl Claim {
  pub fn new(id: u32, left: u32, top: u32, width: u32, height: u32) -> Claim {
    Claim {
      id,
      left,
      top,
      width,
      height,
    }
  }

  pub fn points(&self) -> Vec<Point> {
    let mut points: Vec<Point> = Vec::new();
    for x in 0..self.width {
      for y in 0..self.height {
        let p: Point = (x + self.left, y + self.top);
        points.push(p);
      }
    }
    points
  }
}
